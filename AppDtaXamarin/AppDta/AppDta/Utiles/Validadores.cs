﻿using System;
using System.Text.RegularExpressions;

namespace AppDta.Utiles
{
    static class Validadores
    {

        public static string validateNullOrEmtyToString(string obj)
        {
            var objReturn = "";

            if (!string.IsNullOrEmpty(obj))
            {
                objReturn = obj;
            }

            return objReturn;
        }

        public static double validateStringNullToDouble(string cadena)
        {
            var objReturn = 0.0;

            if (cadena == null || cadena.Equals("null"))
            {
                objReturn = 0.0;

            }
            else if (cadena.Equals(""))
            {
                objReturn = 0.0;
            }
            else {
                objReturn = Double.Parse(cadena);
            }

            return objReturn;
        }

        public static bool ValidationRUC(string ruc)
        {

            if (ruc.Length != 11)
            {
                return true;
            }

            int dig01 = Convert.ToInt32(ruc.Substring(0, 1)) * 5;
            int dig02 = Convert.ToInt32(ruc.Substring(1, 1)) * 4;
            int dig03 = Convert.ToInt32(ruc.Substring(2, 1)) * 3;
            int dig04 = Convert.ToInt32(ruc.Substring(3, 1)) * 2;
            int dig05 = Convert.ToInt32(ruc.Substring(4, 1)) * 7;
            int dig06 = Convert.ToInt32(ruc.Substring(5, 1)) * 6;
            int dig07 = Convert.ToInt32(ruc.Substring(6, 1)) * 5;
            int dig08 = Convert.ToInt32(ruc.Substring(7, 1)) * 4;
            int dig09 = Convert.ToInt32(ruc.Substring(8, 1)) * 3;
            int dig10 = Convert.ToInt32(ruc.Substring(9, 1)) * 2;
            int dig11 = Convert.ToInt32(ruc.Substring(10, 1));

            int suma = dig01 + dig02 + dig03 + dig04 + dig05 + dig06 + dig07 + dig08 + dig09 + dig10;
            int residuo = suma % 11;
            int resta = 11 - residuo;

            int digChk = 0;
            if (resta == 10)
            {
                digChk = 0;
            }
            else if (resta == 11)
            {
                digChk = 1;
            }
            else
            {
                digChk = resta;
            }

            if (dig11 == digChk)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool email_bien_escrito(String email)
        {
            String expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, String.Empty).Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }



    }

}
