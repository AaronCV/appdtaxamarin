﻿using AppDta.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppDta.Utiles
{
    static class Constantes
    {
        public static string CORREO_ELECTRONICO = "dta_autorizaciones@mtc.gob.pe";
        public static string NUMERO_TELEFONICO = "01 6157878";

        public static int COD_TIPO_AGENTES_CARGA_INTERNACIONAL = 35;
        public static int COD_TIPO_AGENCIAS_GENERALES = 36;
        public static int COD_TIPO_OPERADORES_TRANSPORTE_MULTIMODAL = 37;

        public static Usuario usuarioSesion;


        public static async Task<Usuario> getUsuarioSesion() {
            
            List<Usuario> usuarios = await App.Database.GetUsuarioAsync();


            if (usuarios.Count == 0) {
                return usuarioSesion;
            } else {
                return usuarios[0];
            }
            
        }


        public static string getDescConceptoFromCode(int codConcepto)
        {
            var descripcion = "";

            switch (codConcepto)
            {

                case 1:
                    descripcion = "Permiso de operación";
                    break;

                case 7:
                    descripcion = "Fletamento";
                    break;

                case 34:
                    descripcion = "Certificado de  condiciones de seguridad";
                    break;

                case 35:
                    descripcion = "Autorización de Agentes de Carga Internacional";
                    break;

                case 36:
                    descripcion = "Otorgamiento de licencias para las agencias generales";
                    break;

                case 37:
                    descripcion = "Permiso de operación para operadores de transporte multimodal";
                    break;

            }

            return descripcion;
        }


    }

}
