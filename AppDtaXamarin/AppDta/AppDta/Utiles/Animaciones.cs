﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Utiles
{
    class Animaciones
    {
        public static async void animatedMenuItems(StackLayout objStac, Image imgObj)
        {
            try
            {
                if (objStac.IsVisible)
                {
                    await Task.WhenAll(
                       objStac.TranslateTo(0, -10, 250),
                       imgObj.RotateTo(-180, 250),
                       objStac.FadeTo(0, 50)
                   );
                    objStac.IsVisible = false;
                    imgObj.Source = "plus";
                }
                else
                {
                    objStac.IsVisible = true;
                    await Task.WhenAll(
                        objStac.TranslateTo(0, 10, 250),
                        imgObj.RotateTo(0, 250),
                        objStac.FadeTo(30, 50, Easing.SinIn)
                    );
                    imgObj.Source = "minus";
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }

        }
    }
}
