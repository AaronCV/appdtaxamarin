﻿using System.Drawing;
using Xamarin.Forms;

namespace AppDta.Utiles
{
    public static class ColorsApp
    {
        public static Xamarin.Forms.Color GrisShadowForm = Xamarin.Forms.Color.FromHex("E6E6E6");
        public static Xamarin.Forms.Color BlancoForm = Xamarin.Forms.Color.FromHex("fffff");
        public static Xamarin.Forms.Color Lightgrey = Xamarin.Forms.Color.FromHex("D3D3D3");
    }
}
