﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace AppDta.Utiles
{
    public static class Conversores
    {

        public static DateTime ParseDate(string date)
        {
            DateTimeFormatInfo dateFormatProvider = new DateTimeFormatInfo();
            dateFormatProvider.ShortDatePattern = "dd/MM/yyyy";
            return DateTime.Parse(date, dateFormatProvider);
        }

    }
}
