﻿using System;
using System.IO;
using System.Text;

namespace AppDta.Utiles
{
    class ImageConverterHex
    {
        public string convertImage(string fileUri)
        {
            byte[] image = File.ReadAllBytes(@fileUri);
            string hex = Bytes2HexString(image);
            return hex;
        }

        private static byte[] HexString2Bytes(string hexString)
        {
            int bytesCount = (hexString.Length) / 2;
            byte[] bytes = new byte[bytesCount];
            for (int x = 0; x < bytesCount; ++x)
            {
                bytes[x] = Convert.ToByte(hexString.Substring(x * 2, 2), 16);
            }

            return bytes;
        }

        private static string Bytes2HexString(byte[] buffer)
        {
            var hex = new StringBuilder(buffer.Length * 2);
            foreach (byte b in buffer)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            return hex.ToString();
        }
    }
}
