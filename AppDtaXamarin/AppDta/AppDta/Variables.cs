﻿namespace AppDta
{
    public static class Variables
    {
        // https://developers.google.com/maps/documentation/android-api/signup
        public const string GOOGLE_MAPS_ANDROID_API_KEY = "AIzaSyBsbWJ0HvWe6X4tqoBtgj3-rzgBgYguSiA";

        // https://developers.google.com/maps/documentation/ios-sdk/start#step_4_get_an_api_key
        public const string GOOGLE_MAPS_IOS_API_KEY = "AIzaSyC7MvKeuamq1sxEBLLXhXI5Qh2msmY2PNQ";

        // https://msdn.microsoft.com/windows/uwp/maps-and-location/authentication-key
        public const string BING_MAPS_UWP_API_KEY = "your_bing_maps_apikey";
    }
}
