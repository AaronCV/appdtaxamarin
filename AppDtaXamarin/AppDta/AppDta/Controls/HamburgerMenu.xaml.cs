﻿using AppDta.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Controls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HamburgerMenu : MasterDetailPage
    {
        public HamburgerMenu()
        {
            InitializeComponent();
            MyMenu();

        }
        public void MyMenu()
        {
            Detail = new NavigationPage(new MainIntranet());

            List<Menu> menu = new List<Menu>
            {
                new Menu{ Page = 2,MenuTitle="Mi Empresa",  MenuDetail="Mi Empresa",icon="ic_empresa_menu",IsClick = false},
                new Menu{ Page = 3,MenuTitle="Mis trámites",  MenuDetail="Mis trámites",icon="ic_tramites_menu",IsClick = false},
                new Menu{ Page = 4,MenuTitle="Mis permisos",  MenuDetail="Mis permisos",icon="ic_permisos_menu",IsClick = false},
                new Menu{ Page = 5,MenuTitle="Mis naves",  MenuDetail="Mis naves",icon="ic_naves_menu",IsClick = false},
                new Menu{ Page = 6,MenuTitle="Mis pólizas",  MenuDetail="Mis pólizas",icon="ic_polizas_menu",IsClick = false},
                new Menu{ Page = 7,MenuTitle="Contactos",  MenuDetail="Contactos",icon="ic_contactos_menu",IsClick = false},
                new Menu{ Page = 8,MenuTitle="Cerrar Sesión",  MenuDetail="Contactos",icon="logout",IsClick = true}
            };
            ListMenu.ItemsSource = menu;
        }
        private async void ListMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var menu = e.SelectedItem as Menu;
            if (menu != null)
            {
                IsPresented = false;

                var currentUser = await Utiles.Constantes.getUsuarioSesion();

                switch (menu.Page)
                {
                    case 2:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mi_Empresa", "Click", "1");
                        await Navigation.PushAsync(new IntranetDetalleEmpresa());
                        break;

                    case 3:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Tramites", "Click", "1");
                        await Navigation.PushAsync(new IntranetTramites());
                        break;

                    case 4:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Permisos", "Click", "1");
                        await Navigation.PushAsync(new IntranetPermisos());
                        break;

                    case 5:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Naves", "Click", "1");
                        await Navigation.PushAsync(new IntranetNaves(currentUser.administradoPk.ToString(), 1));
                        break;
                    case 6:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Polizas", "Click", "1");
                        await Navigation.PushAsync(new IntranetPolizas());
                        break;

                    case 7:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Contactos", "Click", "1");
                        await Navigation.PushAsync(new IntranetContactos());
                        break;
                    case 8:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Cerrar_Sesion", "Click", "1");
                        cerrarSesion();
                        break;
                }

            }
        } 

        public class Menu
        {
            public string MenuTitle
            {
                get;
                set;
            }
            public string MenuDetail
            {
                get;
                set;
            }

            public ImageSource icon
            {
                get;
                set;
            }

            public bool IsClick
            {
                get;
                set;
            }

            public int Page
            {
                get;
                set;
            }

        }

        async void cerrarSesion()
        {
            var result = await MaterialDialog.Instance.ConfirmAsync("¿Estás seguro de cerrar sesión?", "¡Hey!", "No", "Si");
            if (result.HasValue)
            {
                if (result.Value)
                {
                    //CANCELAR                    
                }
                else
                {
                    var mainpage = (MainPage)BindingContext;
                    await App.Database.DeleteUsuarioAsync();

                    App.Current.MainPage = new NavigationPage(new MainPage())
                    {
                        BarBackgroundColor = Color.FromHex("#F01E41"),
                        BarTextColor = Color.White
                    };
                }
            }
            else
            {
                //SALIR SIN PRESIONAR LOS BOTONES               
            }
        }
    }
}