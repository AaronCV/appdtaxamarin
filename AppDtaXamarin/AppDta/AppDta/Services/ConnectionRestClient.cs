﻿

using AppDta.RestEntities;
using Nancy.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace AppDta.Services
{
    class ConnectionRestClient
    {
        HttpClient client;


        public ConnectionRestClient()
        {
            if (ConstantesWS.USAR_PROXY)
            {
                var proxy = new WebProxy
                {
                    Address = new Uri($"http://{ConstantesWS.PROXY_HOST}:{ConstantesWS.PROXY_PORT}"),
                    BypassProxyOnLocal = false,
                    UseDefaultCredentials = false,
                };

                var httpClientHandler = new HttpClientHandler
                {
                    Proxy = proxy,
                };

                client = new HttpClient(handler: httpClientHandler, disposeHandler: true);
            }
            else {
                client = new HttpClient();
            }
        }

        public async Task<ResponseGeneral> AsyncPostTask(string json, string apiName)
        {
            ResponseToken tkenRequest;
            tkenRequest = getTokenTask();
            return await AsyncPostTask(json, apiName, tkenRequest);
        }

        public async Task<ResponseGeneral> AsyncPostTask(string json, string apiName, ResponseToken objToken)
        {
            ResponseGeneral result = new ResponseGeneral();

            try
            {

                if (objToken.indErro == 1) {
                    result.indErro = 1;
                    result.descErro = 401 + " -> " + objToken.descErro;
                    return result;
                }

                string urlWs = ConstantesWS.URL_DOMAIN + apiName;
                Uri uri = new Uri(string.Format(urlWs, string.Empty));

                

                StringContent content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = null;

                client.DefaultRequestHeaders.Accept.Add(
                    new MediaTypeWithQualityHeaderValue("application/json"));

                client.DefaultRequestHeaders.Authorization =
                                        new AuthenticationHeaderValue("Bearer",
                                        objToken.token);

                response = await client.PostAsync(uri, content);

                string responseBody = await response.Content.ReadAsStringAsync();


                if (response.IsSuccessStatusCode)
                {
                    result.indErro = 0;
                    result.descErro = "OK";
                    result.data = responseBody;

                }
                else
                {

                    result.indErro = 1;
                    result.descErro = response.StatusCode + " -> " + response.RequestMessage;
                }

                return result;

            }
            catch (Exception ex)
            {
                result.indErro = 1;
                result.descErro = ex.Message;

                return result;
            }
        }

        public async Task<ResponseGeneral> AsyncPostXWFUTask(string username, string password, string apiName)
        {
            ResponseToken tkenRequest;
            tkenRequest = getTokenTask();
            return await AsyncPostXWFUTask(username, password, apiName, tkenRequest);
        }

            public async Task<ResponseGeneral> AsyncPostXWFUTask(string username,string password, string apiName, ResponseToken objToken)
        {
            ResponseGeneral result = new ResponseGeneral();

            try {

                if (objToken.indErro == 1)
                {
                    result.indErro = 1;
                    result.descErro = 401 + " -> " + objToken.descErro;
                    return result;
                }

                string urlWs = ConstantesWS.URL_DOMAIN + apiName;
                //string urlWs = "http://192.168.1.62:14807/api-0.0.1-SNAPSHOT/" + apiName;
                Uri uri = new Uri(string.Format(urlWs, string.Empty));            

                var client = new RestClient(uri);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddHeader("Authorization", "Bearer "+ objToken.token);
                request.AddParameter("username", username);
                request.AddParameter("password", password); 
                IRestResponse response = client.Execute(request);

                string responseBody = response.Content;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.indErro = 0;
                    result.descErro = "OK";
                    result.data = responseBody;

                }
                else
                {
                    result.indErro = 1;
                    result.descErro = response.StatusCode + " -> " + response.ErrorMessage;
                }

                return result;

            }
            catch (Exception ex) {
                result.indErro = 1;
                result.descErro = ex.Message;

                return result;
            }

        }

        public async Task<ResponseGeneral> AsyncGetTask(string apiName)
        {
            ResponseToken tkenRequest;
            tkenRequest = getTokenTask();
            return await AsyncGetTask(apiName, tkenRequest);
        }

        public async Task<ResponseGeneral> AsyncGetTask(string apiName, ResponseToken objToken)
        {
            ResponseGeneral result = new ResponseGeneral();

            try
            {

                string urlWs = ConstantesWS.URL_DOMAIN + apiName;
                Uri uri = new Uri(string.Format(urlWs, string.Empty));
              
                HttpResponseMessage response = null;

                client.DefaultRequestHeaders.Authorization =
                                        new AuthenticationHeaderValue("Bearer",
                                        objToken.token);

                response = await client.GetAsync(uri);

                string responseBody = await response.Content.ReadAsStringAsync();


                if (response.IsSuccessStatusCode)
                {
                    result.indErro = 0;
                    result.descErro = "OK";
                    result.data = responseBody;

                }
                else
                {

                    result.indErro = 1;
                    result.descErro = response.StatusCode + " -> " + response.RequestMessage;
                }

                return result;

            }
            catch (Exception ex)
            {
                result.indErro = 1;
                result.descErro = ex.Message;

                return result;
            }
        }

        public ResponseToken getTokenTask()
        {
            ResponseToken result = new ResponseToken();

            try
            {

                string urlWs = ConstantesWS.TOKEN_URL;
                //string urlWs = "http://192.168.1.62:14807/api-0.0.1-SNAPSHOT/" + apiName;
                Uri uri = new Uri(string.Format(urlWs, string.Empty));

                var client = new RestClient(uri);
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("nombreUsuario", ConstantesWS.USER);
                request.AddParameter("clave", ConstantesWS.PASSWORD);
                IRestResponse response = client.Execute(request);

                string responseBody = response.Content;

                if (response.StatusCode == HttpStatusCode.OK)
                {
                    result.indErro = 0;
                    result.descErro = "OK";
                    JObject data = JObject.Parse(responseBody);
                    result.token = (string)data["accessToken"];
                    //result.data = responseBody;

                }
                else
                {
                    result.indErro = 1;
                    result.descErro = response.StatusCode + " -> " + response.ErrorMessage;
                }

                return result;

            }
            catch (Exception ex)
            {
                result.indErro = 1;
                result.descErro = ex.Message;

                return result;
            }

        }
    }
}
