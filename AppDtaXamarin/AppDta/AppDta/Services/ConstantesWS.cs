﻿namespace AppDta.Services
{
    static class ConstantesWS
    {
        /*URLS DISPONIBLES*/

#if DEBUG
        //public const string URL_DOMAIN = "http://192.168.251.126:8080/ApiWsAppDta/api/Consultas/";//TEST    
        //public const string URL_DOMAIN = "http://172.22.9.164:8082/ApiWsAppDta/api/Consultas/";//JHONNY
        public const string URL_DOMAIN = "http://192.168.251.108/ApiWsAppDta/api/Consultas/";//DESARROLLO  
#endif

#if RELEASE
        //public const string URL_DOMAIN = "http://192.168.251.108/ApiWsAppDta/api/Consultas/";//DESARROLLO
        public const string URL_DOMAIN = "http://dta.mtc.gob.pe/ApiWsAppDta/api/Consultas/";//PROD
#endif

        /*APIS*/
        public const string API_REGISTRAR_DENUNCIA = "registrarDenuncias";
        public const string API_LISTAR_ADMINISTRADOS = "filtrarAdministrados";
        public const string API_LISTAR_AUTORIZACIONES = "consultarAutorizaciones";
        public const string API_LISTAR_NAVES_AUTORIZACIONES = "listarNavesPorAdministradoLibre";
        public const string API_LISTAR_ADMINISTRADOS_DENUNCIAS = "listarAdministradoDuncias";

        public const string API_LISTAR_DEPARTAMENTOS = "ubigeos/departamentos/";
        public const string API_LISTAR_SERVICIOS = "tiposServicios/";
        public const string API_LISTAR_PUNTOS_GPS = "coordenadas/administrados/";
        public const string API_VALIDAR_USUARIO = "autenticacion";
        public const string API_DATOS_EMPRESA = "administrado/";
        public const string API_DATOS_TRAMITES = "administrado/";
        public const string API_DATOS_PERMISOS = "administrado/";
        public const string API_DATOS_PERMISOS_NAVES = "permisos/";
        public const string API_DATOS_NAVES = "administrado/";
        public const string API_DATOS_POLIZAS = "administrado/";
        public const string API_DATOS_POLIZAS_NAVES = "polizas/";
        public const string API_DATOS_CONTACTOS = "contactos/";

        /*TOKEN-API*/

#if DEBUG
        //public const string TOKEN_URL = "http://192.168.251.126:8080/ApiWsAppDta/Auth";//TEST
        //public const string TOKEN_URL = "http://172.22.9.164:8082/ApiWsAppDta/Auth";//JHONNY
        public const string TOKEN_URL = "http://192.168.251.108/ApiWsAppDta/Auth";//DESARROLLO
#endif

#if RELEASE
        //public const string TOKEN_URL = "http://192.168.251.108/ApiWsAppDta/Auth";//DESARROLLO
        public const string TOKEN_URL = "http://dta.mtc.gob.pe/ApiWsAppDta/Auth";//PROD
#endif

        public const string USER = "USERDTA";
        public const string PASSWORD = "appdtatokensecurity";

        /*USAR-PROXY*/
        public const bool USAR_PROXY = false; //Activar solamente cuando se ejecuta en la PC del MTC (NUNCA "true" PARA PRODUCCION)
        public const string PROXY_HOST = "mtc-prx01.mtc.gob.pe";
        public const int PROXY_PORT = 8080;
    }
}
