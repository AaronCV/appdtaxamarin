﻿using AppDta.Binds;
using AppDta.Controls;
using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using AppDta.Utiles;
using AppDta.Views;
using Nancy.Extensions;
using Nancy.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {

        AdministradoRest objRequest;
        List<CustomPikerBind> listaFiltros;

        int indFiltro = 0;

        public MainPage()
        {            
            InitializeComponent();
            poblarPickerTipo();
        }
       
        private async  void poblarPickerTipo()
        {
            try
            {
                var listaTipos = new List<CustomPikerBind>{
                                        new CustomPikerBind{ CODE = 1, NAME = "Permiso de operación" },
                                        new CustomPikerBind{ CODE = 7, NAME = "Fletamento" },
                                        new CustomPikerBind{ CODE = 34, NAME = "Certificado de Condiciones de Seguridad" },
                                        new CustomPikerBind{ CODE = 35, NAME = "Agentes de Carga Internacional" },
                                        new CustomPikerBind{ CODE = 36, NAME = "Agencias Generales" },
                                        new CustomPikerBind{ CODE = 37, NAME = "Operadores de Transporte Multimodal" }
                                   };

                listaFiltros = new List<CustomPikerBind> { };

                pickerTipo.Choices = listaTipos;
                pickerTipo.ChoicesBindingName = "NAME";
                pickerTipo.SelectedChoice = "CODE";

                pickerFiltro.Choices = listaFiltros;
                pickerFiltro.ChoicesBindingName = "NAME";
                pickerTipo.SelectedChoice = "CODE";
            }
            catch (Exception ex) {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
         
        }

        private async void OnClickMenuItems(object sender, EventArgs e)
        {
            try
            {
                var card = sender as MaterialCard;
                var flag = Int16.Parse(card.ClickCommandParameter.ToString());

                Page page;

                switch (flag)
                {
                    case 1:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Ubicacion_Administrados", "Click", "1");
                        page = (Page)Activator.CreateInstance(typeof(UbicanosView));
                        await Navigation.PushAsync(page);
                        break;

                    case 2:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Reportar_Caso", "Click", "1");
                        page = (Page)Activator.CreateInstance(typeof(DenunciasView));
                        await Navigation.PushAsync(page);
                        break;
                }

            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
           
        }

        public async void mostrarEntryFiltro(object sender, EventArgs e)
        {
            try
            {
                var pickerObject = (XF.Material.Forms.UI.MaterialTextField)sender;
                var itemSelected = (CustomPikerBind)pickerObject.SelectedChoice;
                indFiltro = itemSelected.CODE;
                switch (itemSelected.CODE)
                {

                    case 1:
                        edtNombreAdministrado.IsVisible = true; //visible
                        edtRucFilter.IsVisible = false;
                        edtNaveMatricula.IsVisible = false;
                        edtNombreNave.IsVisible = false;
                        edtOmi.IsVisible = false;
                        edtNumeroRD.IsVisible = false;
                        break;

                    case 2:
                        edtNombreAdministrado.IsVisible = false;
                        edtRucFilter.IsVisible = true; //visible
                        edtNaveMatricula.IsVisible = false;
                        edtNombreNave.IsVisible = false;
                        edtOmi.IsVisible = false;
                        edtNumeroRD.IsVisible = false;
                        break;

                    case 3:
                        edtNombreAdministrado.IsVisible = false;
                        edtRucFilter.IsVisible = false;
                        edtNaveMatricula.IsVisible = true; //visible
                        edtNombreNave.IsVisible = false;
                        edtOmi.IsVisible = false;
                        edtNumeroRD.IsVisible = false;
                        break;

                    case 4:
                        edtNombreAdministrado.IsVisible = false;
                        edtRucFilter.IsVisible = false;
                        edtNaveMatricula.IsVisible = false;
                        edtNombreNave.IsVisible = true; //visible
                        edtOmi.IsVisible = false;
                        edtNumeroRD.IsVisible = false;
                        break;

                    case 5:
                        edtNombreAdministrado.IsVisible = false;
                        edtRucFilter.IsVisible = false;
                        edtNaveMatricula.IsVisible = false;
                        edtNombreNave.IsVisible = false;
                        edtOmi.IsVisible = true; //visible
                        edtNumeroRD.IsVisible = false;
                        break;

                    case 6:
                        edtNombreAdministrado.IsVisible = false;
                        edtRucFilter.IsVisible = false;
                        edtNaveMatricula.IsVisible = false;
                        edtNombreNave.IsVisible = false;
                        edtOmi.IsVisible = false;
                        edtNumeroRD.IsVisible = true; //visible
                        break;
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
           
        }

        public async void consultarAdministrados(object sender, EventArgs e)
        {
            try
            {
                pickerTipo.HasError = false;
                pickerFiltro.HasError = false;
                edtNombreAdministrado.HasError = false;
                edtRucFilter.HasError = false;
                edtNaveMatricula.HasError = false;

                var pickerObject = new XF.Material.Forms.UI.MaterialTextField();
                var itemSelected = "";

                objRequest = new AdministradoRest();

                pickerObject = (XF.Material.Forms.UI.MaterialTextField)pickerTipo;
                itemSelected = pickerObject.Text;

                if (itemSelected == "")
                {
                    pickerTipo.ErrorText = "Seleccione un tipo";
                    pickerTipo.HasError = true;
                    return;
                }



                pickerObject = (XF.Material.Forms.UI.MaterialTextField)pickerFiltro;
                itemSelected = pickerObject.Text;
                if (itemSelected == "")
                {
                    pickerFiltro.ErrorText = "Debe seleccionar un filtro";
                    pickerFiltro.HasError = true;
                    return;
                }
             

                var stackLayout = (StackLayout)FindByName("stackFiltros");
                var listChilds = stackLayout.Children;
                bool itemResult = false;

                foreach (MaterialTextField item in listChilds)
                {
                    item.HasError = false;
                }

                foreach (MaterialTextField item in listChilds) {
                    if (item.IsVisible) {
                        itemResult = String.IsNullOrEmpty(item.Text);
                        if (itemResult) {
                            item.HasError = true;
                            item.ErrorText = "Ingresar " + item.Placeholder;
                        } 
                        
                        if (item.Text.Length < 4){
                            itemResult = true;
                            item.HasError = true;
                            item.ErrorText = "Debe ingresare por lo menos 4 caracteres";
                        }
                    }
                }

                if (itemResult) {                  
                    return;
                }

                var itemTipoFiltro = (CustomPikerBind)pickerFiltro.SelectedChoice;
                var esNaveBusqueda = false;

                switch (itemTipoFiltro.CODE) {

                    case 1:
                        objRequest.razonSocial = Utiles.Validadores.validateNullOrEmtyToString(edtNombreAdministrado.Text);
                        objRequest.ruc = "";
                        break;

                    case 2:
                        objRequest.ruc = Utiles.Validadores.validateNullOrEmtyToString(edtRucFilter.Text);
                        break;

                    case 3:
                        objRequest.matriculaNave = Utiles.Validadores.validateNullOrEmtyToString(edtNaveMatricula.Text);
                        esNaveBusqueda = true;
                        break;

                    case 4:
                        objRequest.nombreNave = Utiles.Validadores.validateNullOrEmtyToString(edtNombreNave.Text);
                        esNaveBusqueda = true;
                        break;

                    case 5:
                        objRequest.omi = Utiles.Validadores.validateNullOrEmtyToString(edtOmi.Text);
                        esNaveBusqueda = true;
                        break;

                    case 6:
                        objRequest.numeroDocumento = Utiles.Validadores.validateNullOrEmtyToString(edtNumeroRD.Text);
                        break;

                }

                //Limpiar valores
                this.edtNombreAdministrado.Text = "";
                this.edtRucFilter.Text = "";
                this.edtNaveMatricula.Text = "";
                this.edtNombreNave.Text = "";
                this.edtOmi.Text = "";
                this.edtNumeroRD.Text = "";

                var itemTipoAut = (CustomPikerBind)pickerTipo.SelectedChoice;
                objRequest.conceptoFiltro = itemTipoAut.CODE;
                var json = new JavaScriptSerializer().Serialize(objRequest);

                DependencyService.Get<IAnalyticsService>()?.LogEvent("Consultar_Autorizaciones", itemTipoAut.NAME, itemTipoFiltro.NAME);
                await Navigation.PushAsync(new FiltroAdministradosView(json, itemTipoAut.CODE, itemTipoAut.NAME, esNaveBusqueda));
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
            
        }


        public async void OnClickLLamar(object sender, EventArgs e)
        {
            try
            {
                DependencyService.Get<IAnalyticsService>()?.LogEvent("Llamar", "Click", "1");

                PhoneDialerDta diler = new PhoneDialerDta();
                diler.PlacePhoneCall(Constantes.NUMERO_TELEFONICO);
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }           
        }

        public async void OnClickSendMail(object sender, EventArgs e)
        {
            DependencyService.Get<IAnalyticsService>()?.LogEvent("Enviar_un_correo", "Click", "1");

            await Navigation.PushAsync(new MailView());
        }

        public async void OpenSelection(object sender, EventArgs e) {

            try
            {
                var card = sender as MaterialCard;
                var stackCOntent = card.Content as StackLayout;

                var imgRotate = stackCOntent.Children[2] as Image;

                var flag = card.ClickCommandParameter.ToString();

                var stackContent = (StackLayout)FindByName(flag);

                animatedMenuItems(stackContent, imgRotate);
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
            
        }

        protected async void animatedMenuItems(StackLayout objStac,Image imgObj) {

            try
            {
                if (objStac.IsVisible)
                {
                    await Task.WhenAll(
                       objStac.TranslateTo(0, -10, 250),
                       imgObj.RotateTo(-180, 250),
                       objStac.FadeTo(0, 50)
                   );
                    objStac.IsVisible = false;
                    imgObj.Source = "plus";
                }
                else
                {
                    objStac.IsVisible = true;
                    await Task.WhenAll(
                        objStac.TranslateTo(0, 10, 250),
                        imgObj.RotateTo(0, 250),
                        objStac.FadeTo(30, 50, Easing.SinIn)
                    );
                    imgObj.Source = "minus";
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
            
        }

        public async void validarCredenciales(object sender, EventArgs e) {         

            try
            {
                using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Validando usuario, espere unos momentos..."))
                {
                    dialog.MessageText = "Validando credenciales...";

                    if (string.IsNullOrEmpty(edtCorreoAdministrado.Text))
                    {
                        edtCorreoAdministrado.ErrorText = "Ingrese correo.";
                        edtCorreoAdministrado.HasError = true;
                        edtCorreoAdministrado.Focus();
                        return;
                    }

                    if (string.IsNullOrEmpty(edtClave.Text))
                    {
                        edtClave.ErrorText = "Ingrese contraseña.";
                        edtClave.HasError = true;
                        edtClave.Focus();
                        return;
                    }

                    dialog.MessageText = "Conectando al servidor...";

                    var usernamer = edtCorreoAdministrado.Text;
                    var password = edtClave.Text;
                                  
                    await consultarServicioLogin(usernamer, password);

                    dialog.MessageText = "Validación concluida...";
                };
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }

        }

        protected async Task consultarServicioLogin( string usernamer, string password) {
            try {
                var jsonResponse = new ResponseGeneral();
                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncPostXWFUTask(usernamer, password, ConstantesWS.API_VALIDAR_USUARIO);

                if (jsonResponse.indErro == 0)
                {
                    JObject jarr = JObject.Parse(jsonResponse.data);

                    var usuario = jarr.ToObject<UsuarioRest>();


                    if (usuario != null)
                    {

                        var error = Utiles.Validadores.validateNullOrEmtyToString(usuario.errorMessage);

                        if (error.Equals(""))
                        {
                            var usuarioModel = new Usuario();
                            usuarioModel.nombreCompleto = usuario.nombreCompleto;
                            usuarioModel.perfil = usuario.perfil;
                            usuarioModel.razonSocial = usuario.razonSocial;
                            usuarioModel.userPk = int.Parse(usuario.userPk);
                            usuarioModel.administradoPk = int.Parse(usuario.administradoPk);

                            if (cbxGuardarSesion.IsSelected)
                            {
                                var mainpage = (MainPage)BindingContext;
                                await App.Database.SaveUsuarioAsync(usuarioModel);
                            }
                            else {
                                Utiles.Constantes.usuarioSesion = usuarioModel;
                            }

                            DependencyService.Get<IAnalyticsService>()?.LogEvent("Iniciar_Sesion", "Razon_Social", usuarioModel.razonSocial);
                            await Navigation.PushAsync(new HamburgerMenu());
                            await MaterialDialog.Instance.SnackbarAsync(message: "Te Damos La Bienvenido : " + usuario.nombreCompleto,
                                                msDuration: MaterialSnackbar.DurationLong);
                        }
                        else
                        {
                            await MaterialDialog.Instance.SnackbarAsync(message: usuario.errorMessage + " Detalle-> " + usuario.errorDetail,
                                                msDuration: MaterialSnackbar.DurationLong);
                        }
                    }
                }
                else
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                msDuration: MaterialSnackbar.DurationLong);
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }          
        }

        public async void cambioDeFiltrosSegunTipo(object sender, EventArgs e) {

            var selectedChoise = sender as MaterialTextField;
            var itemSelect = (CustomPikerBind)selectedChoise.SelectedChoice;

            pickerFiltro.Choices.Clear();
            pickerFiltro.Text = "";

            switch (itemSelect.CODE) {

                case 1:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 3, NAME = "Matricula de la nave"},
                                        new CustomPikerBind{ CODE = 4, NAME = "Nombre de la nave" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;
                case 7:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 3, NAME = "Matricula de la nave"},
                                        new CustomPikerBind{ CODE = 4, NAME = "Nombre de la nave" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;
                case 34:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 3, NAME = "Matricula de la nave"},
                                        new CustomPikerBind{ CODE = 5, NAME = "OMI" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;
                case 35:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;
                case 36:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;
                case 37:

                    listaFiltros = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "Nombre de administrado" },
                                        new CustomPikerBind{ CODE = 2, NAME = "RUC de administrado" },
                                        new CustomPikerBind{ CODE = 6, NAME = "Número de R.D." }
                                   };

                    break;

                default:
                    pickerFiltro.IsVisible = false;
                    break;

            }
 
            pickerFiltro.Choices = listaFiltros;
            pickerFiltro.ChoicesBindingName = "NAME";
            pickerFiltro.IsVisible = true;

            edtNombreAdministrado.IsVisible = false;
            edtRucFilter.IsVisible = false;
            edtNaveMatricula.IsVisible = false;
            edtNombreNave.IsVisible = false;
            edtOmi.IsVisible = false;
        }

    }

}
