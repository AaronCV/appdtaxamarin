﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Xamarin.Forms;

namespace AppDta.Converters
{
    class EstadoNaveColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Color result = Color.Gray;
            string codEstadoNave = (string)value;
            
            if (!string.IsNullOrEmpty(codEstadoNave))
            {
                switch (codEstadoNave)
                {
                    case "1":
                        result = Color.Gray; break;
                    case "2":
                        result = Color.Purple; break;
                }
            }

            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Color.Black;
        }
    }
}
