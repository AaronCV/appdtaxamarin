﻿using AppDta.Controls;
using AppDta.Data;
using AppDta.Views;
using System;
using System.IO;
using Xamarin.Forms;
using XF.Material.Forms;

namespace AppDta
{
    public partial class App : Application
    {

        static AppDtaDatabase database;

        public static AppDtaDatabase Database 
        {
            get
            {
                if (database == null)
                {
                    database = new AppDtaDatabase(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "AppDta.db3"));
                }
                return database;
            }
        }

        public App()
        {
            InitializeComponent();
            Material.Init(this, "Material.Style");
            validarLogin();
        }

        protected async void validarLogin()
        {
            var sesionActiiva = await App.Database.GetUsuarioAsync();

            if (sesionActiiva.Count != 0)
            {
                var usuario = sesionActiiva[0];
                MainPage = new NavigationPage(new HamburgerMenu())
                {
                    BarBackgroundColor = Color.FromHex("#F01E41"),
                    BarTextColor = Color.White
                };

            }
            else
            {
                MainPage = new NavigationPage(new MainPage())
                {
                    BarBackgroundColor = Color.FromHex("#F01E41"),
                    BarTextColor = Color.White
                };

            }
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
