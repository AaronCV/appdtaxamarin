﻿using AppDta.Models;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppDta.Data
{
    public class AppDtaDatabase
    {
        readonly SQLiteAsyncConnection _database;

        public AppDtaDatabase(string dbPath)
        {
            _database = new SQLiteAsyncConnection(dbPath);
            _database.CreateTableAsync<Usuario>().Wait();
        }

        public Task<List<Usuario>> GetUsuarioAsync()
        {
            return _database.Table<Usuario>().ToListAsync();
        }

        public Task<Usuario> GetUsuarioAsync(int id)
        {
            return _database.Table<Usuario>()
                            .Where(i => i.ID == id)
                            .FirstOrDefaultAsync();
        }

        public Task<int> SaveUsuarioAsync(Usuario usuario)
        {
            if (usuario.ID != 0)
            {
                return _database.UpdateAsync(usuario);
            }
            else
            {
                return _database.InsertAsync(usuario);
            }
        }

        public Task<int> DeleteUsuarioAsync(Usuario usuario)
        {
            return _database.DeleteAsync(usuario);
        }

        public Task<int> DeleteUsuarioAsync()
        {
            return _database.ExecuteAsync("DELETE FROM Usuario");           
        }
    }
}
