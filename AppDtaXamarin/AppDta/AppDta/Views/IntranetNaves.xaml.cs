﻿using AppDta.Binds;
using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetNaves : ContentPage
    {
        int administradoPk = 0;
        string polizaPk = "0";
        string permisoDocPk = "0";

        public IntranetNaves(string codigo,int flagWs)
        {            
            InitializeComponent();

            spnEstadoNaves.IsVisible = false;

            if (flagWs == 1)
            {
                var listaEstados = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 1, NAME = "TODOS" },
                                        new CustomPikerBind{ CODE = 2, NAME = "APTA PARA NAVEGAR" },
                                        new CustomPikerBind{ CODE = 3, NAME = "NO APTA PARA NAVEGAR" }
                                   };

                spnEstadoNaves.IsVisible = false;

                spnEstadoNaves.Choices = listaEstados;
                spnEstadoNaves.ChoicesBindingName = "NAME";
                spnEstadoNaves.SelectedChoice = "CODE";

                buscarNaves(null,null);
            }
            else if (flagWs == 2) 
            {
                polizaPk = codigo;
                buscarNavesPolizas();
            } 
            else if (flagWs == 3) 
            {
                permisoDocPk = codigo;
                buscarNavesPermiso();
            }
        }

        public async void buscarNaves(object sender, EventArgs e)
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando Naves.."))
            {
                try
                {
                    var usuario = await Utiles.Constantes.getUsuarioSesion();
                    var usuarioss = usuario;

                    administradoPk = usuarioss.administradoPk;

                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;


                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_NAVES + administradoPk + "/naves/");


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<NavesRest>>();

                        tramites.ForEach(e => e.isVisibleText = true);

                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + tramites.Count + " naves";
                            lvNaves.ItemsSource = tramites;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

        public async void buscarNavesPolizas()
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando Naves.."))
            {
                try
                {
                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;


                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_POLIZAS_NAVES + polizaPk + "/naves/");


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<NavesRest>>();


                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + tramites.Count + " naves";
                            lvNaves.ItemsSource = tramites;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

        public async void buscarNavesPermiso()
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando Naves.."))
            {
                try
                {
                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;


                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_PERMISOS_NAVES + permisoDocPk + "/naves/");


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<NavesRest>>();


                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + tramites.Count + " naves";
                            lvNaves.ItemsSource = tramites;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

    }
}