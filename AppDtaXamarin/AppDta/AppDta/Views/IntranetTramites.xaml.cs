﻿using AppDta.Binds;
using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetTramites : ContentPage
    {

        int administradoPk = 0;

        public IntranetTramites()
        {
            InitializeComponent();


            var listaEstados = new List<CustomPikerBind> {
                                        new CustomPikerBind{ CODE = 4, NAME = "Pendiente" },
                                        new CustomPikerBind{ CODE = 1, NAME = "Finalizado" },
                                        new CustomPikerBind{ CODE = 0, NAME = "Observado" }
                                   };

            spnEstado.Choices = listaEstados;
            spnEstado.ChoicesBindingName = "NAME";
            spnEstado.SelectedChoice = "CODE";

        }

        public async void buscarTramites(object sender, EventArgs e) {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando tramites.."))
            {
                try
                {
                    var usuario = await Utiles.Constantes.getUsuarioSesion();
                    var usuarioss = usuario;

                    administradoPk = usuarioss.administradoPk;

                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;

                    var selectedChoise = sender as MaterialTextField;
                    var codEsta = (CustomPikerBind)selectedChoise.SelectedChoice;

                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_TRAMITES + administradoPk+ "/tramites/estado/"+ codEsta.CODE);


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<TramitesRest>>();
                        
                        lvTramites.ItemsSource = null;
                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron "+ tramites.Count+" trámites";

                            foreach (TramitesRest item in tramites)
                            {                               
                                item.fechaRegistroSort = Utiles.Conversores.ParseDate(item.fechaRegistro);
                                if (codEsta.CODE == 1) {                                    
                                    item.isVisibleFechNoti = true;
                                    item.isVisibleFechAten = false;                                    
                                    item.isVisibleFechObser = false;
                                    item.isVisibleOficio = false;
                                    item.isVisiblePlazo = false;
                                    item.isVisibleRD = true;
                                } else if (codEsta.CODE == 4) {
                                    item.isVisibleFechNoti = false;
                                    item.isVisibleFechAten = false;
                                    item.isVisibleFechObser = false;
                                    item.isVisibleOficio = false;
                                    item.isVisiblePlazo = true;
                                    item.isVisibleRD = false;
                                } else if (codEsta.CODE == 0) {
                                    item.isVisibleFechNoti = false;
                                    item.isVisibleFechAten = false;
                                    item.isVisibleFechObser = false;
                                    item.isVisibleOficio = false;
                                    item.isVisiblePlazo = false;
                                    item.isVisibleRD = false;
                                }
                            }

                            var listaConvertidaOrdenada = tramites.OrderByDescending(o => o.fechaRegistroSort.TimeOfDay).ThenBy(x => x.fechaRegistroSort.Date)
                                                                                                                      .ThenBy(x => x.fechaRegistroSort.Year);                         

                            lvTramites.ItemsSource = listaConvertidaOrdenada; 
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

    }
}