﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class VisualizadorPdf : ContentPage
    {
        public string uriPdf = "";

        public VisualizadorPdf(string uriPdf)
        {
            uriPdf = "http://docs.google.com/gview?embedded=true&url=" + uriPdf;
            InitializeComponent();
            pdfView.Source = uriPdf;
        }

        void webviewNavigating(object sender, WebNavigatingEventArgs e)
        {
            labelLoading.IsVisible = true;
        }

        void webviewNavigated(object sender, WebNavigatedEventArgs e)
        {
            labelLoading.IsVisible = false;
        }

    }
}