﻿
using AiForms.Dialogs;
using AppDta.RestEntities;
using AppDta.Services;
using Nancy.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FiltroAdministradosView : ContentPage
    {
        AdministradoRest _objeto;
        int codTipoAutorizacion = 0;
        string descripTipoAutori = "";
        bool indiEsNave = false;

        public List<AdministradoRest> listAdministrados;


        public FiltroAdministradosView(string request, int codTipoAut, string descTipoAutori, bool esNave)
        {
            InitializeComponent();
            initialData(request,codTipoAut,descTipoAutori,esNave);
        }

        public async void initialData(string request, int codTipoAut, string descTipoAutori, bool esNave) {
            try
            {
                _objeto = new JavaScriptSerializer().Deserialize<AdministradoRest>(request);

                codTipoAutorizacion = codTipoAut;
                descripTipoAutori = descTipoAutori;
                indiEsNave = esNave;


                using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando información..."))
                {
                    if (esNave) {
                        dialog.MessageText = "Cargando naves...";                        
                    } else {
                        dialog.MessageText = "Cargando administrados...";
                    }                    
                    llamarServicioWeb(request);
                    BindingContext = this;                    
                };
               
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
        }

        public async void llamarServicioWeb(string jsonRequest)
        {
            try
            {
                var jsonResponse = new ResponseGeneral();

                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncPostTask(jsonRequest, ConstantesWS.API_LISTAR_ADMINISTRADOS);


                if (jsonResponse.indErro == 0)
                {
                    JObject obj = JObject.Parse(jsonResponse.data);
                    var jarr = obj["data"].Value<JArray>();
                    //var listaAdministrados = JsonConvert.DeserializeObject<AdministradoRest>(dataString);
                    listAdministrados = jarr.ToObject<List<AdministradoRest>>();

                    int indErro = (int)obj.GetValue("indErro");

                    if (indErro == 0)
                    {
                        if (listAdministrados.Count == 0)
                        {
                            if (indiEsNave)
                            {
                                lbMensaje.Text = "No se encontraron naves";
                            }
                            else
                            {
                                lbMensaje.Text = "No se encontraron administrados";
                            }
                            stackTitle.IsVisible = false;
                            lbMensaje.IsVisible = true;
                            lvAdministrados.IsVisible = false;
                        }
                        else
                        {

                            if (indiEsNave)
                            {
                                stackTitle.IsVisible = true;
                                if (listAdministrados.Count == 1)
                                {
                                    lbTitle.Text = "Se encontró " + listAdministrados.Count + " Nave.";
                                }
                                else
                                {
                                    lbTitle.Text = "Se encontró " + listAdministrados.Count + " Naves.";
                                }

                                lvAdministrados.IsVisible = false;
                                lvNaves.IsVisible = true;

                                lvNaves.ItemsSource = listAdministrados;
                            }
                            else
                            {
                                stackTitle.IsVisible = true;
                                if (listAdministrados.Count == 1)
                                {
                                    lbTitle.Text = "Se encontró " + listAdministrados.Count + " Administrado.";
                                }
                                else
                                {
                                    lbTitle.Text = "Se encontró " + listAdministrados.Count + " Administrados.";
                                }

                                lvNaves.IsVisible = false;
                                lvAdministrados.IsVisible = true;

                                listAdministrados.ForEach(e => e.departamento = e.departamento + "/" + e.provincia);

                                lvAdministrados.ItemsSource = listAdministrados;
                            }

                        }
                    }
                    else 
                    {
                        await DisplayAlert("Respuesta", jsonResponse.descErro, "Ok");
                    } 

                }

                else
                {
                    await DisplayAlert("Respuesta", jsonResponse.descErro, "Ok");
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public async void buscarAutorizaciones(object sender, EventArgs e)
        {
            try
            {
                ListView lvAdmin = (ListView)sender;

                var itemAdmin = (AdministradoRest)lvAdmin.SelectedItem;

                AdministradoRest objRequest = new AdministradoRest();
                objRequest.administradoPk = itemAdmin.administradoPk;
                objRequest.conceptoFiltro = codTipoAutorizacion;

                if (!string.IsNullOrEmpty(_objeto.numeroDocumento))
                {
                    objRequest.numeroDocumento = _objeto.numeroDocumento;
                }

                var json = new JavaScriptSerializer().Serialize(objRequest);
                if (indiEsNave)
                {
                    await Navigation.PushAsync(new ListadoAutorizacionesView(json, codTipoAutorizacion, descripTipoAutori, itemAdmin.matriculaNave, itemAdmin.nombreNave));
                }
                else
                {
                    await Navigation.PushAsync(new ListadoAutorizacionesView(json, codTipoAutorizacion, descripTipoAutori, itemAdmin.ruc, itemAdmin.razonSocial));
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

    }
}