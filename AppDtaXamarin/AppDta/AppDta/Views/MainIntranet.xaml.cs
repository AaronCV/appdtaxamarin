﻿using AppDta.Models;
using AppDta.RestEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainIntranet : ContentPage
    {
        public Usuario currentUser;

        public MainIntranet()
        {
            InitializeComponent();
            mostrarDatosBanerPrincipal();
        }

        protected async void mostrarDatosBanerPrincipal() {

            var usuario = await Utiles.Constantes.getUsuarioSesion();
            currentUser = usuario;

            lbNombre.Text = currentUser.nombreCompleto ;
            lbEmpresa.Text = currentUser.razonSocial;
        }

        public async void OnClickMenuItems(object sender, EventArgs e) {

            try
            {
                var card = sender as MaterialCard;
                var flag = Int16.Parse(card.ClickCommandParameter.ToString());

                Page page;

                switch (flag)
                {
                    case 1: //Mi Empresa
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mi_Empresa", "Click", "1");
                        await Navigation.PushAsync(new IntranetDetalleEmpresa());                        
                        break;

                    case 2: //Mis Tramites
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Tramites", "Click", "1");
                        await Navigation.PushAsync(new IntranetTramites());                                                
                        break;

                    case 3: //Mis Permisos
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Permisos", "Click", "1");
                        await Navigation.PushAsync(new IntranetPermisos());                        
                        break;

                    case 4: //Mis Naves
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Naves", "Click", "1");
                        await Navigation.PushAsync(new IntranetNaves(currentUser.administradoPk.ToString(), 1));                        
                        break;
                    case 5: //Mis Polizas
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Mis_Polizas", "Click", "1");
                        await Navigation.PushAsync(new IntranetPolizas());                        
                        break;

                    case 6: //Contactos
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Contactos", "Click", "1");
                        await Navigation.PushAsync(new IntranetContactos());                        
                        break;
                    case 7:
                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Cerrar_Sesion", "Click", "1");
                        ConfirmarCierreSesion_Clicked();
                        //cerrarSesion();
                        break;
                }

            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }

        }

        async void cerrarSesion() {

            var mainpage = (MainPage)BindingContext;
            await App.Database.DeleteUsuarioAsync();
          
            await Navigation.PushAsync(new MainPage());
        }

        private async void ConfirmarCierreSesion_Clicked()
        {
            var result = await MaterialDialog.Instance.ConfirmAsync("¿Estás seguro de cerrar sesión?", "¡Hey!", "No", "Si");
            if (result.HasValue)
            {
                if (result.Value)
                {
                    //CANCELAR
                }
                else
                {
                    cerrarSesion();                    
                }
            }
            else
            {
                //SALIR SIN PRESIONAR LOS BOTONES
            }
        }

    }
}