﻿using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetPolizas : ContentPage
    {
        int administradoPk = 0;

        public IntranetPolizas()
        {
            InitializeComponent();
            buscarPolizas();
        }

        public async void buscarPolizas()
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando pólizas.."))
            {
                try
                {
                    var usuario = await Utiles.Constantes.getUsuarioSesion();
                    var usuarioss = usuario;

                    administradoPk = usuarioss.administradoPk;

                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;


                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_POLIZAS + administradoPk + "/polizas/");


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<PolizasRest>>();


                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + tramites.Count + " pólizas";
                            lvPolizas.ItemsSource = tramites;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

        public async void buscarNaves(object sender, EventArgs e) {
            ListView lvPoliza = (ListView)sender;

            var itemPoliza = (PolizasRest)lvPoliza.SelectedItem;

            await Navigation.PushAsync(new IntranetNaves(itemPoliza.polizaPk, 2));
        }

    }
}