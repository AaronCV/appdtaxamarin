﻿using AppDta.Utiles;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MailView : ContentPage
    {
        public MailView()
        {
            InitializeComponent();
            txtTo.Text = Constantes.CORREO_ELECTRONICO;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        async void btnSend_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                List<string> toAddress = new List<string>();
                toAddress.Add(txtTo.Text);
                await SendEmail(txtSubject.Text, txtBody.Text, toAddress);
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }           
        }

        public async Task SendEmail(string subject, string body, List<string> recipients)
        {
            try
            {
                var message = new EmailMessage
                {
                    Subject = subject,
                    Body = body,
                    To = recipients,
                };
                await Email.ComposeAsync(message);
            }
            catch (FeatureNotSupportedException fbsEx)
            {
                // Email is not supported on this device  
                await MaterialDialog.Instance.SnackbarAsync(message: fbsEx.Message,
                                       msDuration: MaterialSnackbar.DurationLong);
            }
            catch (Exception ex)
            {
                // Some other exception occurred  
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                       msDuration: MaterialSnackbar.DurationLong);
            }
        }

    }
}