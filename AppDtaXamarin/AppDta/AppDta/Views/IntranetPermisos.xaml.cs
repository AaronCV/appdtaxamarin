﻿using AppDta.Binds;
using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetPermisos : ContentPage
    {
        int administradoPk = 0;

        public IntranetPermisos()
        {
            InitializeComponent();            
            buscarPermisos();
        }

        public async void buscarPermisos()
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando permisos.."))
            {
                try
                {
                    var usuario = await Utiles.Constantes.getUsuarioSesion();
                    var usuarioss = usuario;

                    administradoPk = usuarioss.administradoPk;

                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;

                  
                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_PERMISOS + administradoPk + "/permisos/estado/" + 1);


                    if (jsonResponse.indErro == 0)
                    {
                        JArray objJson = JArray.Parse(jsonResponse.data);

                        var tramites = objJson.ToObject<List<PermisosRest>>();


                        if (tramites.Count != 0)
                        {
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + tramites.Count + " permisos";

                            foreach (PermisosRest item in tramites) {
                                if (item.urlResolucion != null)
                                {
                                    item.issVisibleRd = true;
                                }
                                else 
                                {
                                    item.issVisibleRd = false;
                                }
                            }

                            lvPermisos.ItemsSource = tramites;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

        public async void buscarNaves(object sender, EventArgs e)
        {
            ListView lvPermiso = (ListView)sender;

            var itemPermiso = (PermisosRest)lvPermiso.SelectedItem;

            await Navigation.PushAsync(new IntranetNaves(itemPermiso.documentoOficialPk, 3));
        }

        public async void mostrarPdf(object sender, EventArgs args)
        {
            var card = sender as MaterialButton;
            var uri =card.CommandParameter.ToString();

            await Navigation.PushAsync(new VisualizadorPdf(uri));
        }

        public async void descargarPdf(object sender, EventArgs args)
        {
            var card = sender as MaterialButton;
            var uri = card.CommandParameter.ToString();

            await Launcher.OpenAsync(uri);
        }

    }   
}