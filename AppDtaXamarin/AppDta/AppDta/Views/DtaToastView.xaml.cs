﻿using AiForms.Dialogs.Abstractions;
using Xamarin.Forms.Xaml;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DtaToastView : ToastView
    {
        public DtaToastView()
        {
            InitializeComponent();
        }

        // define appearing animation
        public override void RunPresentationAnimation() { }

        // define disappearing animation
        public override void RunDismissalAnimation() { }

        // define clean up process.
        public override void Destroy() { }
    }
}