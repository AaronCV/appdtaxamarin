﻿using AppDta.Models;
using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;


using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetDetalleEmpresa : ContentPage
    {

        List<SociosRest> listaContactos = new List<SociosRest>();
        int administradoPk = 0;

        public IntranetDetalleEmpresa()
        {
            InitializeComponent();            
            cargarDatosEmpresa();
        }

        public async void cargarDatosEmpresa() {

             using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando detalle de la empresa.."))
            {
                try
                {
                    var usuario = await Utiles.Constantes.getUsuarioSesion();
                    administradoPk = usuario.administradoPk;

                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_EMPRESA+administradoPk);


                    if (jsonResponse.indErro == 0)
                    {
                        JObject objJson = JObject.Parse(jsonResponse.data);

                        var administrado = objJson.ToObject<AdministradoRest>();

                        var error = Utiles.Validadores.validateNullOrEmtyToString(administrado.errorMessage);

                        if (error.Equals(""))
                        {
                            lbRuc.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.ruc);
                            lbRazonSocial.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.razonSocial);
                            lbTelefonos.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.telefono);
                            lbDireccion.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.direccion);
                            lbEmail.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.email);
                            lbEmailNotificacion.Text = Utiles.Validadores.validateNullOrEmtyToString(administrado.emailNotificacion);

                            if (administrado.lstSocios.Count != 0) {
                                listaContactos = administrado.lstSocios;
                            }
                        }
                        else {
                            await MaterialDialog.Instance.SnackbarAsync(message: administrado.errorMessage+" -> "+administrado.errorDetail,
                                                    msDuration: MaterialSnackbar.DurationLong);
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };

        }

        public async void mostrarSocios(object sender, EventArgs e) {
            try
            {
                var card = sender as MaterialCard;
                var stackCOntent = card.Content as StackLayout;

                var imgRotate = stackCOntent.Children[2] as Image;

                var flag = card.ClickCommandParameter.ToString();

                var stackContent = (StackLayout)FindByName(flag);

                Utiles.Animaciones.animatedMenuItems(stackContent, imgRotate);

                lvSocios.ItemsSource = listaContactos;
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
        }
    }
}