﻿using AiForms.Dialogs;
using AiForms.Dialogs.Abstractions;
using AppDta.RestEntities;
using AppDta.Services;
using AppDta.Utiles;
using Nancy.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListadoAutorizacionesView : ContentPage
    {
        AdministradoRest _objeto;
        string tipoAutorizacion = "";
        string ruc = "";
        string razonSocial = "";
        List<AutorizacionRest> listTramites = new List<AutorizacionRest>();
        List<AdministradoRest> listNaves = new List<AdministradoRest>();

        public ListadoAutorizacionesView(string request, int codTipoAutorizacion, string nameTipoAutorizacion, string rucAdmin, string nombreAdministrado)
        {
            InitializeComponent();
            initialData(request, codTipoAutorizacion, nameTipoAutorizacion, rucAdmin,nombreAdministrado);
            initialTabView();
        }

        private async void initialData(string request, int codTipoAutorizacion, string nameTipoAutorizacion, string rucAdmin, string nombreAdministrado) {
            try
            {
                _objeto = new JavaScriptSerializer().Deserialize<AdministradoRest>(request);

                tipoAutorizacion = nameTipoAutorizacion;
                ruc = rucAdmin;
                razonSocial = nombreAdministrado;

                lbRazonSocial.Text = razonSocial;
                lbRuc.Text = ruc;

                lbAutorizacion.Text = tipoAutorizacion;

                if (!string.IsNullOrEmpty(_objeto.numeroDocumento))
                {
                    lnNumeroRD.IsVisible = true;
                    lbNumeroRD.Text = _objeto.numeroDocumento;
                }

                //Solo Mostrara Naves en los que sean distintos de estos tipos:

                bool bMostrarNaves = (!(codTipoAutorizacion == Constantes.COD_TIPO_AGENTES_CARGA_INTERNACIONAL ||
                                        codTipoAutorizacion == Constantes.COD_TIPO_AGENCIAS_GENERALES ||
                                        codTipoAutorizacion == Constantes.COD_TIPO_OPERADORES_TRANSPORTE_MULTIMODAL));

                this.btnTapNave.IsVisible = bMostrarNaves;

                using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando información..."))
                {
                    dialog.MessageText = "Cargando autorizaciones...";
                    llamarServicioWeb(request);

                    if (bMostrarNaves)
                    {
                        dialog.MessageText = "Cargando naves...";
                        llamarServicioWebNaves(request);   
                    }
                };              

            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public async void llamarServicioWeb(string jsonRequest)
        {
            try
            {
                var jsonResponse = new ResponseGeneral();

                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncPostTask(jsonRequest, ConstantesWS.API_LISTAR_AUTORIZACIONES);

                if (jsonResponse.indErro == 0)
                {
                    JObject obj = JObject.Parse(jsonResponse.data);
                    var jarr = obj["data"].Value<JArray>();
                    listTramites = jarr.ToObject<List<AutorizacionRest>>();

                    if (listTramites.Count == 0)
                    {
                        lbMensajeTramite.IsVisible = true;
                        return;
                    }

                    lvAutorizaciones.IsVisible = true;
                    lbMensajeTramite.IsVisible = false;

                    foreach (AutorizacionRest item in listTramites)
                    {
                        item.issVisibleRd = !string.IsNullOrEmpty(item.urlResolucion);
                        //item.concepto = Constantes.getDescConceptoFromCode(Int32.Parse(a.concepto));

                        var fechaInicioSaneada = Utiles.Validadores.validateNullOrEmtyToString(item.fechaInicio);
                        var soloDate = "";
                        if (fechaInicioSaneada != "")
                        {
                            soloDate = fechaInicioSaneada.Split(' ')[0];
                            item.fechaInicio = soloDate;
                        }
                        else
                        {
                            item.fechaInicio = fechaInicioSaneada;
                        }

                        var fechaFinSaneada = Utiles.Validadores.validateNullOrEmtyToString(item.fechaFin);
                        soloDate = item.fechaFin.Split(' ')[0];
                        item.fechaFin = fechaInicioSaneada;

                        if (fechaFinSaneada != "")
                        {
                            soloDate = fechaFinSaneada.Split(' ')[0];
                            item.fechaFin = soloDate;
                        }
                        else
                        {
                            item.fechaFin = "Indefinida";
                        }

                    }

                    lvAutorizaciones.ItemsSource = listTramites;
                    btnTapTramite.Text = " Resolución (" + listTramites.Count + ") ";
                }

                else
                {
                    await DisplayAlert("Respuesta", jsonResponse.descErro, "Ok");
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public void initialTabView() {
            btnTapTramite.ButtonType = MaterialButtonType.Flat;
            btnTapTramite.BackgroundColor = Utiles.ColorsApp.GrisShadowForm;
            btnTapNave.ButtonType = MaterialButtonType.Text;
            btnTapNave.BackgroundColor = Utiles.ColorsApp.BlancoForm;
            tabTramites.IsVisible = true;
            tabNaves.IsVisible = false;
        }

        public void ChangeTabClick(object sender, EventArgs e) {

            var btnPress = sender as MaterialButton;
            var parametro = btnPress.CommandParameter;
            int indicador = Int16.Parse(parametro.ToString());

            if (indicador == 2) {
                btnPress.ButtonType = MaterialButtonType.Flat;
                btnTapTramite.ButtonType = MaterialButtonType.Text;
                btnPress.BackgroundColor = Utiles.ColorsApp.GrisShadowForm;
                btnTapTramite.BackgroundColor = Utiles.ColorsApp.BlancoForm;
                tabTramites.IsVisible = false;
                tabNaves.IsVisible = true;

            }else if(indicador == 1) {
                btnPress.ButtonType = MaterialButtonType.Flat;
                btnTapNave.ButtonType = MaterialButtonType.Text;
                btnPress.BackgroundColor = Utiles.ColorsApp.GrisShadowForm;
                btnTapNave.BackgroundColor = Utiles.ColorsApp.BlancoForm;
                tabTramites.IsVisible = true;
                tabNaves.IsVisible = false;
            }
        }

        public async void llamarServicioWebNaves(string jsonRequest)
        {
            try
            {
                var jsonResponse = new ResponseGeneral();

                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncPostTask(jsonRequest, ConstantesWS.API_LISTAR_NAVES_AUTORIZACIONES);

                if (jsonResponse.indErro == 0)
                {
                    JObject obj = JObject.Parse(jsonResponse.data);

                    int indErro = (int)obj.GetValue("indErro");

                    if (indErro == 0)
                    {

                        var jarr = obj["data"].Value<JArray>();
                        listNaves = jarr.ToObject<List<AdministradoRest>>();


                        if (listNaves.Count == 0)
                        {
                            lbMensajeNave.IsVisible = true;
                            return;
                        }

                        lvNaves.IsVisible = true;
                        lbMensajeNave.IsVisible = false; ;


                        lvNaves.ItemsSource = listNaves;

                        btnTapNave.Text = " Naves (" + listNaves.Count + ") ";
                    }
                    else {
                        await DisplayAlert("Respuesta Naves", obj.GetValue("descErro").ToString(), "Ok");
                    }
                    
                }

                else
                {
                    await DisplayAlert("Respuesta Naves", jsonResponse.descErro, "Ok");
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
        }

        public async void mostrarPdf(object sender, EventArgs args)
        {
            var card = sender as MaterialButton;
            var uri = card.CommandParameter.ToString();

            await Navigation.PushAsync(new VisualizadorPdf(uri));
        }

        public async void descargarPdf(object sender, EventArgs args)
        {
            var card = sender as MaterialButton;
            var uri = card.CommandParameter.ToString();

            await Launcher.OpenAsync(uri);
        }
    }
}