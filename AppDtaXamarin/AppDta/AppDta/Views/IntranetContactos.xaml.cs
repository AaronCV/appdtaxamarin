﻿using AppDta.RestEntities;
using AppDta.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntranetContactos : ContentPage
    {
        public IntranetContactos()
        {
            InitializeComponent();
            buscarContactos();
        }

        public async void buscarContactos()
        {

            using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando contactos.."))
            {
                try
                {
                    lbMensaje.IsVisible = false;
                    stackTitle.IsVisible = false;


                    var jsonResponse = new ResponseGeneral();
                    ConnectionRestClient con = new ConnectionRestClient();
                    jsonResponse = await con.AsyncGetTask(ConstantesWS.API_DATOS_CONTACTOS);

                    if (jsonResponse.indErro == 0)
                    {
                        /*
                        string sTestData = @"[ {
                            ""errorMessage"" : null,
                            ""errorDetail"" : null,
                            ""userPk"" : null,
                            ""username"" : ""dta_autorizaciones@mtc.gob.pe"",
                            ""nombreCompleto"" : ""Dirección de Autorizaciones de Transporte Acuático"",
                            ""dni"" : null,
                            ""perfil"" : ""DTA"",
                            ""telefono"" : ""01 6157800"",
                            ""ruc"" : null,
                            ""administradoPk"" : null,
                            ""razonSocial"" : null,
                            ""secureToken"" : null
                        } ]";
                        JArray objJson = JArray.Parse(sTestData);*/

                        JArray objJson = JArray.Parse(jsonResponse.data);
                        var Lstcontactos = objJson.ToObject<List<ContactoRest>>();

                        var listaDeSoloVisibles = new List<ContactoRest>();

                        if (Lstcontactos.Count != 0)
                        {
                            foreach (ContactoRest item in Lstcontactos) {
                                listaDeSoloVisibles.Add(item);

                                /*
                                if (item.perfil.Equals("ROLE_DIRECTOR")) {
                                    item.perfil = "DTA";
                                    item.telefono = "01 6157800";
                                    listaDeSoloVisibles.Add(item);
                                }*/
                            }
                            stackTitle.IsVisible = true;
                            lbTitle.Text = "Se encontraron " + listaDeSoloVisibles.Count + " contactos";
                            lvContactos.ItemsSource = listaDeSoloVisibles;
                        }
                        else
                        {
                            lbMensaje.IsVisible = true;
                        }
                    }
                    else
                    {
                        await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                    msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                catch (Exception ex)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                            msDuration: MaterialSnackbar.DurationLong);
                }
            };
        }

    }
}