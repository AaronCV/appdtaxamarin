﻿using AiForms.Dialogs;
using AiForms.Dialogs.Abstractions;
using AppDta.Binds;
using AppDta.RestEntities;
using AppDta.Services;
using AppDta.Utiles;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.GoogleMaps;
using Xamarin.Forms.Xaml;
using XF.Material.Forms.UI.Dialogs;

namespace AppDta.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UbicanosView : ContentPage
    {

        public List<DepartamentoRest> listDepartamento;
        public List<ServicioRest> listServivcios;
        public List<AdministradosGpsRest> listAdministrados;

        Location lastLocation;
        Location currentSelectedLocation;

        public UbicanosView()
        {
            InitializeComponent();
            initialData();
        }
        private async void initialData() {
            try
            {
                map.PinClicked += async (sender, e) => {
                    currentSelectedLocation = new Location();
                    currentSelectedLocation.Latitude = e.Pin.Position.Latitude;
                    currentSelectedLocation.Longitude = e.Pin.Position.Longitude;
                };


                cargarInformacionPrincipal();
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
        }

        public async void cargarInformacionPrincipal() {
            try
            {
                using (var dialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Cargando..."))
                {
                    dialog.MessageText = "Cargando ubicación actual...";
                    await obtenerPosicionActual();
                    dialog.MessageText = "Cargando departamentos...";
                    await obtenerDepartamentos();
                    dialog.MessageText = "Cargando servicios";
                    await obtenerServicios(null, null);
                };

            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public async Task obtenerDepartamentos() {

            try
            {
                var jsonResponse = new ResponseGeneral();
                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncGetTask(ConstantesWS.API_LISTAR_DEPARTAMENTOS);


                if (jsonResponse.indErro == 0)
                {
                    JArray jarr = JArray.Parse(jsonResponse.data);

                    listDepartamento = jarr.ToObject<List<DepartamentoRest>>();


                    if (listDepartamento.Count != 0)
                    {

                        var listaTipos = new List<CustomPikerBind>();
                        var numCant = 1;
                        foreach (DepartamentoRest dep in listDepartamento)
                        {
                            listaTipos.Add(new CustomPikerBind { CODE_STRING = dep.departamentosPk, NAME = dep.nombre+" ("+ dep.cantidad + ")" });
                        }

                        List<CustomPikerBind> sortedList = listaTipos.OrderBy(o => o.NAME).ToList();

                        var itemBorrar = sortedList.Find(item => item.NAME.Equals("PERU")); ;
                        sortedList.Remove(itemBorrar);

                        pickerDepartamento.Choices = sortedList;
                        pickerDepartamento.ChoicesBindingName = "NAME";
                        pickerDepartamento.SelectedChoice = "CODE_STRING";
                    }
                }
                else
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                msDuration: MaterialSnackbar.DurationLong);
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }
        }

        public async Task obtenerServicios(object sender, EventArgs e)
        {
            try
            {
                var jsonResponse = new ResponseGeneral();

                ConnectionRestClient con = new ConnectionRestClient();
                jsonResponse = await con.AsyncGetTask(ConstantesWS.API_LISTAR_SERVICIOS);


                if (jsonResponse.indErro == 0)
                {
                    JArray jarr = JArray.Parse(jsonResponse.data);

                    listServivcios = jarr.ToObject<List<ServicioRest>>();

                    if (listServivcios.Count != 0)
                    {
                        var listaServicio = new List<CustomPikerBind>();

                        foreach (ServicioRest serv in listServivcios)
                        {
                            listaServicio.Add(new CustomPikerBind { CODE_STRING = serv.tserviciosPk, NAME = serv.descripcion });
                        }

                        pickerServicio.Choices = listaServicio;
                        pickerServicio.ChoicesBindingName = "NAME";
                        pickerServicio.SelectedChoice = "CODE_STRING";
                    }
                }
                else
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                msDuration: MaterialSnackbar.DurationLong);
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public async Task obtenerPosicionActual()
        {

            try
            {
                var request = new GeolocationRequest(GeolocationAccuracy.High);
                lastLocation = await Geolocation.GetLocationAsync(request);

                if (lastLocation != null)
                {
                    map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(new Position(lastLocation.Latitude, lastLocation.Longitude), 12d);
                    var posiActual = new Position(lastLocation.Latitude, lastLocation.Longitude);
                    
                    CameraPosition cam = new CameraPosition(posiActual, 10f);
                    var  update = CameraUpdateFactory.NewCameraPosition(cam);
                    map.AnimateCamera(update);                  
                }
                else
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: "No se obtuvieron coordenadas, intente denuevo",
                                      msDuration: MaterialSnackbar.DurationLong);
                }
            }
            catch (Exception ex)
            {
                Loading.Instance.Hide();
                await MaterialDialog.Instance.SnackbarAsync(message: "No es posible obtener la ubicación actual, por favor verifique si esta encendido su GPS  -> " + ex.Message,
                                          msDuration: MaterialSnackbar.DurationLong);
            }

        }


        private async void obtenerUbicaciones(object sender, EventArgs e)
        {
            var loadingDialog = await MaterialDialog.Instance.LoadingDialogAsync(message: "Buscando administrados...", lottieAnimation: "LottieLogo1.json");
            try
            {
                map.Pins.Clear();
                map.Pins.Remove(map.SelectedPin);

                
                var jsonResponse = new ResponseGeneral();

                ConnectionRestClient con = new ConnectionRestClient();

                var pickerObject = new XF.Material.Forms.UI.MaterialTextField();
                var itemSelected = "";


                pickerObject = (XF.Material.Forms.UI.MaterialTextField)pickerDepartamento;
                itemSelected = pickerObject.Text;

                if (itemSelected == "")
                {
                    pickerDepartamento.ErrorText = "Seleccione un departamento";
                    pickerDepartamento.HasError = true;
                    await loadingDialog.DismissAsync();
                    return;
                }

                var pickerObjectServ = new XF.Material.Forms.UI.MaterialTextField();
                var itemSelectedServ = "";


                pickerObjectServ = (XF.Material.Forms.UI.MaterialTextField)pickerServicio;
                itemSelectedServ = pickerObjectServ.Text;

                if (itemSelectedServ == "")
                {
                    pickerServicio.ErrorText = "Seleccione un servicio";
                    pickerServicio.HasError = true;
                    await loadingDialog.DismissAsync();
                    return;
                }

                var codDepa = (CustomPikerBind)pickerDepartamento.SelectedChoice;
                var codServicio = (CustomPikerBind)pickerServicio.SelectedChoice;

                jsonResponse = await con.AsyncGetTask(ConstantesWS.API_LISTAR_PUNTOS_GPS + codDepa.CODE_STRING + "/" + codServicio.CODE_STRING);

                if (jsonResponse.indErro == 0)
                {
                    JArray jarr = JArray.Parse(jsonResponse.data);

                    listAdministrados = jarr.ToObject<List<AdministradosGpsRest>>();

                    Position startPos = new Position();

                    if (listAdministrados.Count != 0)
                    {
                        var intCO = 0;
                        foreach (AdministradosGpsRest item in listAdministrados)
                        {
                            var latitudLimpia = Utiles.CleanString.UseStringBuilderCoordenadas(item.latitud);
                            var longitudLimpia = Utiles.CleanString.UseStringBuilderCoordenadas(item.longitud);

                            var latitudConvert = Utiles.Validadores.validateStringNullToDouble(latitudLimpia);
                            var longitudConvert = Utiles.Validadores.validateStringNullToDouble(longitudLimpia);

                            startPos = new Position(latitudConvert, longitudConvert);

                            var marcaEnMapa = new Pin();

                            marcaEnMapa.Label = Utiles.Validadores.validateNullOrEmtyToString(item.razonSocial);
                            marcaEnMapa.Address = Utiles.Validadores.validateNullOrEmtyToString(item.direccion);
                            marcaEnMapa.Tag = "Administrado";
                            marcaEnMapa.Position = startPos;

                            map.Pins.Add(marcaEnMapa);

                            intCO++;
                        }

                        DependencyService.Get<IAnalyticsService>()?.LogEvent("Ubicanos", codDepa.NAME, codServicio.NAME);
                        map.InitialCameraUpdate = CameraUpdateFactory.NewPositionZoom(startPos, 12d);

                        CameraPosition cam = new CameraPosition(startPos, 5f);
                        var update = CameraUpdateFactory.NewCameraPosition(cam);
                        await map.AnimateCamera(update, null);

                    }
                    else {
                        await MaterialDialog.Instance.SnackbarAsync(message: "No se encontraron administrados en el departamento.",
                                                                        msDuration: MaterialSnackbar.DurationLong);
                    }
                }
                else
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: jsonResponse.descErro,
                                                msDuration: MaterialSnackbar.DurationLong);
                }

                await loadingDialog.DismissAsync();
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
                await loadingDialog.DismissAsync();
            }            
        }

        private async void activarPanel(object sender, EventArgs e) {
            try
            {
                if (cvPanelFiltros.IsVisible)
                {
                    await Task.WhenAll(
                     cvPanelFiltros.TranslateTo(0, -10, 250),
                     cvPanelFiltros.FadeTo(0, 50)
                   );

                    optionMenuBuscarAdministrados.IconImageSource = "filterplus";
                    cvPanelFiltros.IsVisible = false;
                }
                else
                {

                    await Task.WhenAll(
                      cvPanelFiltros.TranslateTo(0, 10, 250),
                      cvPanelFiltros.FadeTo(30, 50, Easing.SinIn)
                  );

                    optionMenuBuscarAdministrados.IconImageSource = "filterinusoutline";
                    cvPanelFiltros.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        public async void mostrarRutaEnMapa(object sender, EventArgs e) {
            try
            {
                if (currentSelectedLocation == null)
                {
                    await MaterialDialog.Instance.SnackbarAsync(message: "Debe seleccionar un comcercio en el mapa",
                                              msDuration: MaterialSnackbar.DurationLong);
                    return;
                }

                double latitudCurrentSelected = currentSelectedLocation.Latitude;
                double longitudCurrentSelected = currentSelectedLocation.Longitude;

                if (Device.RuntimePlatform == Device.iOS)
                {

                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    // open the maps app directly -12.045943, -77.098681

                    await Launcher.OpenAsync("https://www.google.com/maps/dir/?api=1&destination=" + latitudCurrentSelected +", "+ longitudCurrentSelected+ "&travelmode=driving");
                }
                else if (Device.RuntimePlatform == Device.UWP)
                {

                }
            }
            catch (Exception ex)
            {
                await MaterialDialog.Instance.SnackbarAsync(message: ex.Message,
                                        msDuration: MaterialSnackbar.DurationLong);
            }            
        }

        protected override bool OnBackButtonPressed()
        {
            return true;
        }

    }
}