﻿using System;
using SQLite;

namespace AppDta.Models
{
    public class Usuario
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }

        public int userPk { get; set; }
        public string nombreCompleto { get; set; }
        public string perfil { get; set; }
        public string ruc { get; set; }
        public int administradoPk { get; set; }
        public string razonSocial { get; set; }
    }
}
