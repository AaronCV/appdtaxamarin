﻿namespace AppDta.RestEntities
{
    class ResponseGeneral
    {

        public int indErro { get; set; }
        public string descErro { get; set; }
        public string parametroExtra { get; set; }
        public string data { get; set; }

    }

}
