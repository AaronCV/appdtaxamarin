﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public class NavesRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public string navePk { get; set; }
        public string nombre { get; set; }
        public string matricula { get; set; }
        public string estadoDeOperatividad { get; set; }
        public string listaPolizas { get; set; }
        public string listaPolizasResp { get; set; }
        public string doficialNavePk { get; set; }
        public string estado { get; set; }
        public string capMax { get; set; }
        public string arqueoBruto { get; set; }
        public bool isVisibleText { get; set; }
    }
}
