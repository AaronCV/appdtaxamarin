﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public class SociosRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public int? socioPk { get; set; }
        public string nombreCompleto { get; set; }
        public string dni { get; set; }
        public string tipoSocio { get; set; }
    }
}
