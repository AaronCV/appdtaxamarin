﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public class UsuarioRest
    {
        public string errorMessage { get; set; }
		public string errorDetail { get; set; }
		public string userPk { get; set; }
		public string username { get; set; }
		public string nombreCompleto { get; set; }
		public string dni { get; set; }
		public string perfil { get; set; }
		public string telefono { get; set; }
		public string ruc { get; set; }
		public string administradoPk { get; set; }
		public string razonSocial { get; set; }
		public string secureToken { get; set; }
	}
}
