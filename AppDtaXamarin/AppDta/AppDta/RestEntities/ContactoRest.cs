﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public  class ContactoRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public string userPk { get; set; }
        public string username { get; set; }
        public string nombreCompleto { get; set; }
        public string dni { get; set; }
        public string perfil { get; set; }
        public string telefono { get; set; }
        public string ruc { get; set; }
        public string administradoPk { get; set; }
        public string razonSocial { get; set; }
        public string secureToken { get; set; }

        public ContactoRest()
        {
            this.errorMessage = "";
            this.errorDetail = "";
            this.userPk = "";
            this.username = "";
            this.nombreCompleto = "";
            this.dni = "";
            this.perfil = "";
            this.telefono = "";
            this.ruc = "";
            this.administradoPk = "";
            this.razonSocial = "";
            this.secureToken = "";
        }
    }
}
