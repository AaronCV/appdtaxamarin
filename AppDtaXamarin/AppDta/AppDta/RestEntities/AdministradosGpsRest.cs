﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public class AdministradosGpsRest
    {
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string razonSocial { get; set; }
        public string direccion { get; set; }

        public AdministradosGpsRest()
        {
            this.latitud = "";
            this.longitud = "";
            this.razonSocial = "";
            this.direccion = "";
        }
    }
}
