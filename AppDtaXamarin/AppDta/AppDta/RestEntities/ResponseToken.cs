﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    class ResponseToken
    {
        public string token { get; set; }
        public int indErro { get; set; }
        public string descErro { get; set; }
    }
}
