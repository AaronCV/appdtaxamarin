﻿namespace AppDta.RestEntities
{
    public class AutorizacionRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public int? documentoOficialPk { get; set; }
        public string tramiteSaltaPk { get; set; }
        public string numeroResolucion { get; set; }
        public string concepto { get; set; }
        public string conceptoDesc { get; set; } //
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string ambito { get; set; }
        public string trafico { get; set; }
        public string transporte { get; set; }
        public string modalidadServicio { get; set; }
        public string urlResolucion { get; set; } //descargar RD
        public string administrado { get; set; }
        public string user { get; set; }
        public string estado { get; set; }
        public string diasPorVencer { get; set; }
        public string ruc { get; set; }
        public string nro_licencia { get; set; }
        public string nro_documento { get; set; }
        public string rutas { get; set; } //
        public bool? issVisibleRd { get; set; }

        public AutorizacionRest()
        {
            this.errorMessage = "";
            this.errorDetail = "";
            this.documentoOficialPk = 0;
            this.tramiteSaltaPk = "";
            this.numeroResolucion = "";
            this.concepto = "";
            this.fechaInicio = "";
            this.fechaFin = "";
            this.ambito = "";
            this.trafico = "";
            this.transporte = "";
            this.modalidadServicio = "";
            this.urlResolucion = "";
            this.administrado = "";
            this.user = "";
            this.estado = "";
            this.diasPorVencer = "";
            this.ruc = ruc;
            this.nro_licencia = "";
            this.nro_documento = "";
        }
    }
}
