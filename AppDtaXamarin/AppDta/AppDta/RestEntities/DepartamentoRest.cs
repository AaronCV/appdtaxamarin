﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.Services
{
    public class DepartamentoRest
    {
        public string departamentosPk { get; set; }
        public string nombre { get; set; }
        public string cantidad { get; set; }

        public DepartamentoRest()
        {
            this.departamentosPk = "";
            this.nombre = "";
            this.cantidad = "";
        }
    }
}
