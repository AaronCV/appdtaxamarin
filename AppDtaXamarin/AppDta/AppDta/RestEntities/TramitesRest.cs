﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    public class TramitesRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public string tramitePk { get; set; }
        public string administradoPk { get; set; }
        public string conceptoPk { get; set; }
        public string administradoRazonSocial { get; set; }
        public string usuarioSolicitud { get; set; }
        public string emailUsuarioSolicitud { get; set; }
        public string evaluador { get; set; }
        public string concepto { get; set; }
        public string fechaRegistro { get; set; }
        public string fechaFinalizado { get; set; }
        public string numStd { get; set; }
        public string numResolucion { get; set; }
        public string urlResolucion { get; set; }

        public string descSubConcepto { get; set; }
        public string estadoTramite { get; set; }
        public string pronunciamiento { get; set; }
        public string fechaNotificacion { get; set; }
        public string fechaObservacion;
        public string oficio { get; set; }
        public string diasTranscurridos { get; set; }
        public string plazo { get; set; }

        public DateTime fechaRegistroSort { get; set; }

        public bool isVisibleFechNoti { get; set; }
        public bool isVisibleFechAten { get; set; }
        public bool isVisibleFechObser { get; set; }
        public bool isVisiblePlazo { get; set; }
        public bool isVisibleRD { get; set; }
        public bool isVisibleOficio { get; set; }
    }


}
