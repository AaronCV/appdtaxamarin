﻿using System.Collections.Generic;
using Xamarin.Forms.Internals;

namespace AppDta.RestEntities
{
    public class AdministradoRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public int? administradoPk { get; set; }
        public string ruc { get; set; }
        public string razonSocial { get; set; }
        public string direccion { get; set; }
        public string telefono { get; set; }
        public string email { get; set; }
        public string paginaWeb { get; set; }
        public string emailNotificacion { get; set; }
        public string capitalReal { get; set; }
        public string moneda { get; set; }
        public string latitud { get; set; }
        public string longitud { get; set; }
        public string matriculaNave { get; set; }
        public string nombreNave { get; set; }
        public string omi { get; set; }
        public int? conceptoFiltro { get; set; }
        public string estadoOperatividadNave { get; set; }
        public int cant_aut { get; set; }
        public string departamento { get; set; }
        public string provincia { get; set; }
        public string capMax { get; set; }
        public string arqueoBruto { get; set; }
        public string modalidadServicio { get; set; }
        public string nro_documento { get; set; }
        public string estado_nave { get; set; }
        public string cod_estado_nave { get; set; }
        public string estado_pago_primas { get; set; }
        public string numeroDocumento { get; set; } //RD para buscar
        public List<SociosRest> lstSocios { get; set; }

        public AdministradoRest()
        {
            this.errorMessage = "";
            this.errorDetail = "";
            this.administradoPk = 0;
            this.ruc = "";
            this.razonSocial = "";
            this.direccion = "";
            this.telefono = "";
            this.email = "";
            this.paginaWeb = "";
            this.emailNotificacion = "";
            this.capitalReal = "";
            this.moneda = "";
            this.latitud = "";
            this.longitud = "";
            this.matriculaNave = "";
            this.nombreNave = "";
            this.conceptoFiltro = 0;
            this.omi = "";
            this.estadoOperatividadNave = "";
            this.departamento = "";
            this.provincia = "";
            this.cant_aut = 0;
            this.capMax = "";
            this.arqueoBruto = "";
            this.modalidadServicio = "";
            this.nro_documento = "";
            this.estado_nave = "";
            this.cod_estado_nave = "";
            this.estado_pago_primas = "";
            this.numeroDocumento = "";
            lstSocios = new List<SociosRest>();            
        }
    }
}
