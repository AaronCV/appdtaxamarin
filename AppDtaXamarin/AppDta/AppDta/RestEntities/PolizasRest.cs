﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppDta.RestEntities
{
    class PolizasRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public string polizaPk { get; set; }
        public string inicioVigencia { get; set; }
        public string finVigencia { get; set; }
        public string tipoPoliza { get; set; }
        public string numeroPoliza { get; set; }
        public string aseguradora { get; set; }
        public string estadoPoliza { get; set; }
        public string monto { get; set; }
        public string administrado { get; set; }
        public string nombreAseguradora { get; set; }
        public string diasPorVencer { get; set; }
        public string observacionesEstadoPago { get; set; }
        public string pagoPrima { get; set; }
    }
}
