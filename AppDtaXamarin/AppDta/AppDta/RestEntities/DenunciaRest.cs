﻿namespace AppDta.RestEntities
{

    class DenunciaRest
    {
        public string dnAltitudVh { get; set; }
        public string dnComentarioVh { get; set; }
        public string dnDepartamento { get; set; }
        public string dnDistritoVh { get; set; }
        public int dnEstadoAnonimatoNu { get; set; }
        public string dnLatitudVh { get; set; }
        public string dnMailVh { get; set; }
        public int dnMotivoDenunciaVh { get; set; }
        public string dnNombreDemandandoVh { get; set; }
        public string dnNombreDemandanteVh { get; set; }
        public string dnNombreFotoVh { get; set; }
        public string dnNumeroDocumentoVh { get; set; }
        public string dnProvinciaVh { get; set; }
        public string dnReferenciaLugarVh { get; set; }
        public string dnRucVh { get; set; }
        public string dnTelefonoVh { get; set; }
        public string dnTieneCoordenadasNu { get; set; }
        public int dnTieneUbigeoNu { get; set; }
        public int dnTipoDocumentoNu { get; set; }
        public string alfFotoHex { get; set; }

        public DenunciaRest()
        {
            this.dnAltitudVh = "";
            this.dnComentarioVh = "";
            this.dnDepartamento = "";
            this.dnDistritoVh = "";
            this.dnEstadoAnonimatoNu = 0;
            this.dnLatitudVh = "0.0";
            this.dnMailVh = "0.0";
            this.dnMotivoDenunciaVh = 0;
            this.dnNombreDemandandoVh = "";
            this.dnNombreDemandanteVh = "";
            this.dnNombreFotoVh = "";
            this.dnNumeroDocumentoVh = "";
            this.dnProvinciaVh = "";
            this.dnReferenciaLugarVh = "";
            this.dnRucVh = "";
            this.dnTelefonoVh = "";
            this.dnTieneCoordenadasNu = "";
            this.dnTieneUbigeoNu = 0;
            this.dnTipoDocumentoNu = 0;
            this.alfFotoHex = "";
        }
    }
}
