﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms.Internals;

namespace AppDta.RestEntities
{
    class PermisosRest
    {
        public string errorMessage { get; set; }
        public string errorDetail { get; set; }
        public string documentoOficialPk { get; set; }
        public string tramiteSaltaPk { get; set; }
        public string numeroResolucion { get; set; }
        public string concepto { get; set; }
        public string conceptoDesc { get; set; }
        public string fechaInicio { get; set; }
        public string fechaFin { get; set; }
        public string ambito { get; set; }
        public string trafico { get; set; }
        public string transporte { get; set; }
        public string modalidadServicio { get; set; }
        public string urlResolucion { get; set; }
        public string administrado { get; set; }
        public string user { get; set; }
        public string estado { get; set; }
        public string estadoDesc { get; set; }
        public string diasPorVencer { get; set; }
        public string ruc { get; set; }
        public string nro_licencia { get; set; }
        public string nro_documento { get; set; }

        public bool? issVisibleRd { get; set; }

        public PermisosRest()
        {
            this.issVisibleRd = true;
        }
    }
}
